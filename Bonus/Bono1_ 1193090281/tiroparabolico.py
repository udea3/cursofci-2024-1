import numpy as np
import matplotlib.pyplot as plt

class tiroParabolico:
    """Clase que me permite calcular y graficar el movimiento de
    tiro parabolico: 
    inputs:
    -_angulo: ingresa (int o float) valor del ángulo de lanzamiento
    -_vel_inicial: ingresa (int o float) valor de la velocidad inicial
    -_pos_inicial: ingresa (int o float) valor de la posición inicial
    -_alt_inicial: ingresa (int o float) valor de la altura inicial
    -_g: ingresa (int o float) valor de la gravedad (en la tierra 9.8m/s²) """

    def __init__(self,_angulo=0,_vel_inicial=0,_pos_inicial=0,_alt_inicial=0,_g=0,_tiempo=0):
        self.angulo= _angulo
        self.vel_inicial=_vel_inicial
        self.pos_inicial=_pos_inicial
        self.alt_inicial= _alt_inicial
        self.g= _g
        self.t=_tiempo
        self.tiempo= np.linspace(0,self.t,10)

    def vel_x(self):
        return self.vel_inicial*np.cos(self.angulo) 

    def vel_y(self):
        return self.vel_inicial*np.sin(self.angulo) + self.g*self.tiempo
        
    
    def tiempo_vuelo(self):
        return print(-(self.vel_y() +np.sqrt((self.vel_y())**2 - 2*self.g*self.alt_inicial))/self.g)


    
    def posiciones(self): 
        x= self.vel_inicial*np.cos(self.angulo)*self.tiempo
        y= ((self.vel_inicial*np.sin(self.angulo)*self.tiempo) + ((1/2)*self.g*((self.tiempo)**2))+self.alt_inicial)
        return [x,y]


    def grafica(self): 
         
         plt.plot(self.posiciones()[0],self.posiciones()[1],"go",label="Trayectoria del movimiento parabólico")
         plt.grid()
         plt.xlabel("posicion eje X")
         plt.ylabel("posicion eje Y")
         plt.legend()
         plt.savefig("Tiroparabolico1.png")
         plt.show

class tiroParabolico2(tiroParabolico):
    def __init__(self,_angulo,_vel_inicial,_pos_inicial,_alt_inicial,_g,tiempo,ax):
        super().__init__(_angulo,_vel_inicial,_pos_inicial,_alt_inicial,_g,tiempo)

        self.ax= ax
    def pos_x(self):
        posx=(self.vel_inicial*np.sin(self.angulo) + ((1/2)*self.ax*(self.tiempo)**2))
        plt.plot(posx,self.posiciones()[1],"go",label="Trayectoria del movimiento parabólico")
        plt.grid()
        plt.xlabel("posicion eje X")
        plt.ylabel("posicion eje Y")
        plt.legend()
        plt.savefig("Tiroparabolico1.png")
        plt.show


