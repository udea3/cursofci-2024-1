import numpy as np
from tiroParabolico import tiroParabolico, tiroParabolico2

if __name__=="__main__":

    #tiro1 = tiroParabolico(45, 20, [10, -15]) # Ángulo de 45 grados, velocidad inicial de 20 m/s y posición inicial [10,-15] m
    #tiro1.grafico_posicion()

    tiro = tiroParabolico2(30, 20, [-3, 4],-5)
    tiro.grafico_posicion()