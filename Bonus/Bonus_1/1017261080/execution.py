from tiro_parabolico import tiroParabolico


if __name__ =="__main__":
    theta = 45
    v = 30
    h0 = 20
    x0 = 0
    g = -9.81
    lanzamiento1 = tiroParabolico(theta,v,h0,x0,g)
    t = lanzamiento1.vueloTiempo()
    lanzamiento1.posGraph(t)