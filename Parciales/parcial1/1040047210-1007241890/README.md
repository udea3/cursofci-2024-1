# Parcial1


## Contenido

Contiene los siguientes archivos:

- clases.py en el que se define una clase RKG la cual usa el método runge kutta generalizado para la solucion de EDO, contiene también una clase RKGA, que se creó usando herencia de clases, para la solución de ecuaciones diferenciales acopladas.

- herencia.py en el que se hace uso de la clase RKG para solucionar la ecuacion diferencial dy/dx=x-y. Se hace un gráfico de la solución numérica y uno donde se hace uso de la solución analitica para observar la convergencia de la solución.

- lorentz.py en el que se hace uso de la clase RKGA con las constantes de RK4 (runge-kutta orden 4) para solucionar el sistema de ecuaciones acopladas para el atractor de Lorentz (se soluciona para x,y,z). El código devuelve una grafica 3D de las coordenadas (x,y,z) y gráficas x vs y, x vs z, y vs z según las condiciones iniciales indicadas.

- requeriments.txt (contiene las librerias con sus verisones usadas en el proyecto)

Adicionalmene el repositorio contiene una carpeta con las gráficas resultantes de la ejecución del archivo execution.py y las resultantes de la ejecución del archivo execution2.py teniendo en cuenta 4 condiciones iniciales distintas.


## Consideraciones particulares

El parámetro c_a en la clase RKG Y RKGA debe ser una matriz organizada según la mnemotécnica del cuadro de Butcher. Puede usarse como ejemplo los valores de matriz que se indicaron en el archivo lorentz.py, específicos para el método runge kutta de orden 4.

## Contribuidores

Este proyecto fue desarrollado para el parcial 1 del curso de física computacional I del Instituto de Física, Universidad de Antioquia, por el equipo conformado por:

Caterine Bedoya (@caterine.bedoyab)

Jhonatan Jurado (@JhonatanJ)