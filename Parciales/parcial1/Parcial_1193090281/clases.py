import numpy as np


class RKG:
    """Esta clase calcula solucion de ecuaciones diferenciales 
    ordinarias de primer grado"""

    def __init__(self, func,  xfinal, h, order, alpha, beta, omega, y0, x0):
        """
        func: es la ecuación diferencial a resolver (Función)
        y0: condicion inical de la variable dependiente (float)
        x0: condicion incial de la variable independiente (float)
        xfinal: Valor finald de la variable independiente (float)
        h: tamaño del paso (float)
        order: el orden del método RungeKuta escogido (int)
        alpha: array con los coeficiente alpha del RungeKutade orden escogido (Array)
        beta: array con los coeficiente beta del RungeKuta de orden escogido (array)
        omega: array con los coeficientes omega del Rungekutta """
        self.func = func
        self.y0 = y0
        self.x0 = x0
        self.xfinal = xfinal
        self.h = h
        self.order = order
        self.alpha = alpha
        self.beta = beta
        self.omega = omega

    def fun_beta(self):
          b= self.beta
          n= self.order
          b_= np.zeros((n,n))
          k = 0
          for i in range(n):
              for j in range(n-1):
                  if j<i: 
                      b_[i][j]= b[k]
                      k+= 1 
          return b_ 
    
    def ks(self, xi, yi):
        """Calcula los factores k """
        n = self.order
        k = np.zeros(n)
        k1 = self.h*self.func(xi,yi)
        k[0] = k1
        a = np.array(self.alpha)
       
        if len(a) != n:
            print("Las constantes alpha están mal definidas, revisar el contenido del array") 
      
        for i  in range(1, n):
          b1=0
          for j in range(1, n-1):
            b1+= self.fun_beta()[i,j]* k[j]
          k[i] = self.h*(self.func(xi + a[i], yi +self.h*b1))
        return k

    
    
    def soluciones(self):
        om = self.omega
        xi = self.x0
        yi= self.y0
        xfinal= self.xfinal
        muestra= int((xfinal-xi)/self.h)
        xs= np.linspace(xi,xfinal,muestra)
        ys= []
        ys.append(yi)
        con=[] 
        con= con.append(0)
        for i in xs[:-1]:
           ks= self.ks(i,ys[-1])
           con=0
           for j in range(self.order):
               con+=om[j]*ks[j]
           ycont = ys[-1]+ con
           ys.append(ycont)
        return (xs,ys)
    


class RGKA(RKG):
    def __init__(self, func, xfinal, h, order, alpha, beta, omega, sigma, rho, bt, y0, x0, z0,):
      super().__init__(func, xfinal, h, order, alpha, beta, omega, y0, x0)
      self.z0 = z0
      self.tfinal = xfinal
      self.sigma = sigma
      self.rho = rho
      self.bt = bt

    def ks(self,ti, xi, yi, zi):
        """Calcula los factores k """
        n = self.order
        k = np.zeros((3,n))
        a = self.alpha
     
        mb = self.fun_beta()

        if len(a) != n:
             print("Las constantes alpha están mal definidas, revisar el contenido del array")

        om = self.omega
        x = 0
        y = 0
        z = 0
        for i  in range(0, n):
            b1 = np.dot(mb,k[0])
            b2 = np.dot(mb,k[1])
            b3 = np.dot(mb,k[2])
            bx = 0
            by = 0
            bz = 0
            for j in range(len(b1)):
              bx += b1[j]
              by += b2[j]
              bz += b3[j]
            fx, fy, fz = self.func(ti+ a[i]*self.h, xi + bx, yi + by,zi +bz , self.sigma, self.rho, self.bt)
            k[0][i] = self.h* fx
            k[1][i] = self.h* fy
            k[2][i] = self.h* fz
            x +=  om[i]* k[0][i]
            y +=  om[i]* k[1][i]
            z +=  om[i]* k[2][i]
        return x, y, z
    
    def soluciones_acopladas(self):
        
        #tfinal =self.tfinal
        muestra= int((self.tfinal)/self.h)
        tamaño= np.linspace(0,self.tfinal,muestra)
        ys = np.zeros(len(tamaño))
        xs =np.zeros(len(tamaño))
        zs = np.zeros(len(tamaño))
        ys[0] = self.y0
        xs[0] = self.x0
        zs[0] =self.z0

        t, dt = 0, self.h
        x0, y0, z0 = xs[0], ys[0], zs[0]
          
        for i in range(1, muestra):
          x, y, z = self.ks(t, x0, y0, z0)
          ys[i] = ys[i-1] + y
          xs[i] = xs[i-1] + x
          zs[i] = zs[i-1] + z 
          x0, y0, z0 =xs[i], ys[i], zs[i]
          t += dt
        return xs,ys,zs








        
